#coding: UTF-8

import cv2
import numpy as np
from PIL import Image
import os

file_path = os.path.dirname(__file__)

# 学習するイメージの保存場所
path = file_path + '../../dataset'

recognizer = cv2.face.LBPHFaceRecognizer_create()
detector = cv2.CascadeClassifier(file_path + "../haarcascade_frontalface_default.xml")

def getImagesAndLabels(path):

    image_paths = [os.path.join(path,f) for f in os.listdir(path) if f.split('.')[-1] == "jpg"]
    face_samples=[]
    ids = []

    for image_path in image_paths:
        PIL_img = Image.open(image_path).convert('L')
        img_numpy = np.array(PIL_img,'uint8')

        id = int(os.path.split(image_path)[-1].split(".")[1])
        faces = detector.detectMultiScale(img_numpy)

        for (x,y,w,h) in faces:
            face_samples.append(img_numpy[y:y+h,x:x+w])
            ids.append(id)

    return face_samples,ids

print ("\n-----学習中-----")
faces,ids = getImagesAndLabels(path)
recognizer.train(faces, np.array(ids))

# モデルを保存する
recognizer.write(file_path + '../../trainer/trainer.yml')

print("\n-----学習終了(学習個数 : {0})------".format(len(np.unique(ids))))
