#coding: UTF-8

import cv2
import os

file_path = os.path.dirname(__file__)

cam = cv2.VideoCapture(0)
cam.set(3, 640) # カメラの横幅を設定
cam.set(4, 480) # カメラの縦幅を設定

face_detector = cv2.CascadeClassifier(file_path + '../haarcascade_frontalface_default.xml')

# 読み込むユーザのIDを設定させる
face_id = input('\n 認識するユーザのIDを入力してください。 ==>  ')
if not(face_id.isdecimal()):
       print("\nIDには数値以外は使用できません。")
       cam.release()
       cv2.destroyAllWindows()
       exit()

print("\n -----処理中-----")

count = 0 # 顔を読み込む枚数

while(True):

    ret, img = cam.read()
    img = cv2.flip(img, -1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_detector.detectMultiScale(gray, 1.3, 5)

    for (x,y,w,h) in faces:

        cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 2)     
        count += 1

        # 読み込んだ画像を保存する
        cv2.imwrite(file_path + "../../dataset/User." + str(face_id) + '.' + str(count) + ".jpg", gray[y:y+h,x:x+w])

        cv2.imshow('image', img)

    k = cv2.waitKey(100) & 0xff # ESCを押したときに終了
    if k == 27:
        break
    elif count >= 30: # 指定した回数を過ぎたとき、処理終了
         break

print("\n -----処理終了-----")
cam.release()
cv2.destroyAllWindows()


