#coding: UTF-8

import cv2
import numpy as np
import os
import slackweb

file_path = os.path.dirname(__file__)

# 通知用slack URL
slack = slackweb.Slack(url=os.environ['CAMELA_SLACK'])

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read(file_path + '../trainer/trainer.yml')
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath);

font = cv2.FONT_HERSHEY_SIMPLEX

id = 0

# idと同じindexの位置に、idに紐づく名前を入力する
names = ['None', 'Temma', 'Temma', 'None', 'None', 'None'] 

cam = cv2.VideoCapture(0)
cam.set(3, 640) # カメラの横幅を設定
cam.set(4, 480) # カメラの縦幅を設定

min_width = 0.1*cam.get(3)
min_height = 0.1*cam.get(4)

while True:

    ret, img =cam.read()
    img = cv2.flip(img, -1)

    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale( 
        gray,
        scaleFactor = 1.2,
        minNeighbors = 5,
        minSize = (int(min_width), int(min_height)),
       )

    for(x,y,w,h) in faces:

        cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)

        id, confidence = recognizer.predict(gray[y:y+h,x:x+w])

        # 信頼度が0~100であった場合、信頼度を表示
        if (confidence < 100):
            id = names[id]
            confidence = "  {0}%".format(round(100 - confidence))
        # それ以外であった場合、本人である可能性は限りなく低い
        else:
            id = "unknown"
            confidence = "  {0}%".format(round(100 - confidence))
            # Slackに通知する
            slack.notify(text="不審者を検出しました")
        
        cv2.putText(img, str(id), (x+5,y-5), font, 1, (255,255,255), 2)
        cv2.putText(img, str(confidence), (x+5,y+h-5), font, 1, (255,255,0), 1)  
    
    cv2.imshow('camera',img) 

    k = cv2.waitKey(10) & 0xff # ESCを押したとき、処理終了
    if k == 27:
        break

print("\n-----処理終了-----")
cam.release()
cv2.destroyAllWindows()
